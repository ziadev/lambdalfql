(defproject lambdalfql "0.1.0-SNAPSHOT"
  :title "FIXME: write title"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license { :name "Eclipse Public License"
             :url "http://www.eclipse.org/legal/epl-v10.html" }
  :min-lein-version "2.0.0"

  ; Dependencies that will be included in the AMP - other dependencies should go in the appropriate profile below
  :dependencies [[cmsql-grammar "0.1.3-SNAPSHOT"]
                 ]
  :profiles { :dev { :plugins [[lein-amp "0.4.0"]] }
              :provided { :dependencies [
                                         [org.clojure/clojure "1.6.0"]
                                         [org.clojars.lambdalf/lambdalf "0.2.0-SNAPSHOT"]
                                        ] }}
  :source-paths    ["src/clojure"]
  :resource-paths  ["src/resource"]
  :amp-source-path "src/amp")
