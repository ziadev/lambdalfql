(ns lambdalfql.core
  (:require [cmsql-grammar.parsers.parseast :as parser]
            [alfresco.core :as c]
            [alfresco.auth :as a]
            [alfresco.transact :as at]
            [alfresco.auth :as aa]
            [alfresco.nodes :as an]
            [alfresco.model :as m]
            )
    (:import [java.lang.String]
             [org.alfresco.service.cmr.repository StoreRef]
             [org.alfresco.service.cmr.search SearchService]
             [org.alfresco.cmis CMISQueryService CMISQueryOptions]))

(use '[clojure.string :only (join split)])

(defn make-properties [columns values]
  (let [columns columns
        values values
        properties (new java.util.HashMap (apply assoc {} (interleave columns values)))
        ]
    ;(println "columns: " columns)
    ;(println "values: " values)
    ;(println "properties: " properties)
    properties))

;(defn get-property [field result]
;  (. (. result getPropertyByQueryName field) getFirstValue))

(defn convert->map [columns properties]
  ;(println "properties" properties)
  (loop [result {}
         columns columns]
    (if (not (seq columns)) result
      (let [first-column (first columns)
            property (get properties first-column)]
        ;(println "first-column:" first-column "property:" property)
        (recur (assoc result first-column property) (rest columns))))
    )
  )

(defn build-result-list [columns query-results]
    (loop [result ()
           prime query-results]
      (if (not (seq prime)) result
        (let [first (convert->map columns (.getValues (first prime)))]
          (recur (conj result first) (rest prime))))))

(defn build-select-query [columns type where-clause]
  (let [comma-delimited-columns (interpose "," columns)]
  (str "SELECT " (apply str comma-delimited-columns) " FROM " type " " where-clause)))

(defn exec-select [statement & [session-name]]
  (let [parsed (parser/parse-tree statement)
        type (:table-name parsed)
        columns (:column-names parsed)
        where-clause (str "WHERE " (join " " (flatten (:where parsed))))
        select-statement (build-select-query columns type where-clause)
        qr (cmis-query select-statement)
        count (.length qr)]
    ;(println "parsed:" parsed)
    ;(println "type:" type)
    ;(println "columns:" columns)
    ;(println "where-clause:" where-clause)
    ;(println "count" count)
    ;(println "temp:" select-statement)
    (build-result-list columns qr)
    ))

(defn ^SearchService search-service
  []
  (.getSearchService (c/alfresco-services)))

(defn ^CMISQueryService cmis-query-service
  []
  (.getCMISQueryService (c/alfresco-services)))

(defn query
  "Returns all the results of a search"
  ([q]
     (query StoreRef/STORE_REF_WORKSPACE_SPACESSTORE
            SearchService/LANGUAGE_LUCENE q))
  ([store lang q]
     (with-open [rs (.query (search-service) store lang q)]
       (.getNodeRefs rs))))

(defn cmis-query
  "Returns all the results of a cmis query search"
  ([q]
     (.query (cmis-query-service) q))
  ([store q]
   (.query (cmis-query-service) (CMISQueryOptions. q store)))
  )

;(at/in-ro-tx-as (aa/admin)
;                (with-open [rs (cmis-query "select cmis:name, cmis:objectId from cmis:folder where cmis:name = 'Company Home'")]
;                  (.getNodeRefs rs)))

;(at/in-ro-tx-as (aa/admin)
;                (with-open [rs (cmis-query StoreRef/STORE_REF_WORKSPACE_SPACESSTORE "select cmis:name, cmis:objectId from cmis:folder where cmis:name = 'Company Home'")]
;                  (.getNodeRefs rs)))

(at/in-ro-tx-as (aa/admin)
               (exec-select "select cmis:name, cmis:objectId from cmis:folder where cmis:name = 'Company Home'"))

(at/in-ro-tx-as (aa/admin)
                (exec-select "select cmis:name, cmis:objectId from cmis:document where cmis:name like 'activities%'"))

(at/in-ro-tx-as (aa/admin)
                (exec-select "select cmis:name, cmis:description, cmis:objectId from cmis:folder where cmis:name like 'C%'"))

